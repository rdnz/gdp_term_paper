---
title: Seminararbeit zu "Ghosts of Departed Proofs"
author: Philipp Zander
lang: en
documentclass: scrartcl
papersize: a4
bibliography: acm.bib
csl: journal-of-the-acm.csl
---

The violation of preconditions causes a large majority of cases of
incorrect semantics including run time errors. [@noonan2018] describes
a technique to design library APIs leveraging the type system to
ensure that any preconditions are satisfied.

Consider a Haskell library providing a type `Map key value`{.haskell}
representing an immutable finite map (sometimes called a dictionary)
from keys of type `key`{.haskell} to values of type
`value`{.haskell}. This library would provide functions to insert key
value pairs and a function `lookup`{.haskell} which is specified to
lookup the value at a key in the map if the key is in the map.

```haskell
-- | Lookup the value at a key in the map if the key is in the map.
lookup ::
  Ord key =>
  key ->
  Map key value ->
  value
```

This is an example for a function assuming a precondition as mentioned
in its specification. The library would provide a function to check
this precondition.

```haskell
-- | Is the key a member of the map?
member ::
  Ord key =>
  key ->
  Map key value ->
  Bool
```

Leaving the responsibility to establish `lookup`{.haskell}'s
precondition to the library's user is too brittle.

```haskell
-- | Lookup the value at a key in the map if the key is in the map.
lookup ::
  Ord key =>
  key ->
  Map key value ->
  Maybe value
```

By returning the option type `Maybe`{.haskell} forces the user to
realize the possiblity of a precondition violation and forces them to
handle it. This is the way the widely used `container` package
chose. It is more robust but if the user knows that the precondition
can never be violated, they cannot reasonably handle a violated
precondition.

The fact that you cannot express propositions about variables through
types because you cannot refer to variables in types is the
fundamental obstacle to expressing the precondition of
`lookup`{.haskell} through its type. Let us try to solve this problem
writing a library that builds upon the `containers` package.

```haskell
module Lib
  (
    Named (),
    Member (),
    member,
    lookup,
  )
  where

import Prelude hiding (lookup)
import qualified Data.Map.Lazy as M

data Named name a = Named a

data Member keyName mapName = Member

member ::
  Ord key =>
  Named keyName key ->
  Named mapName (M.Map key value) ->
  Maybe (Member keyName mapName)
member (Named key) (Named map)
  | M.member key map = Just Member
  | otherwise = Nothing

lookup ::
  Ord key =>
  Named keyName key ->
  Named mapName (M.Map key value) ->
  Member keyName mapName ->
  value
lookup (Named key) (Named map) _proof =
  case M.lookup key map of
    Just value -> value
    Nothing -> error "lookup cannot fail"
```

The type wrapper type `Named`{.haskell} attaches a type variable
`name`{.haskell} to a term variable (that is, a normal variable, not a
type variable). The idea is to express propositions about variables of
type `Named name a`{.haskell} through types by referring to them by
the type variable `name`{.haskell}. Variable of any type can be
wrapped into the `Named`{.haskell} type as you can see.

`lookup`{.haskell} does not return a `Maybe`{.haskell}
anymore. It demands an argument of type `Member keyName
mapName`{.haskell} instead. This is how we express the proposition
that the key passed to `lookup`{.haskell} is in the map passed to
`lookup`{.haskell} through `lookup`{.haskell}'s type. That is, we
managed to express `lookup`{.haskell}'s precondition through its
type. Notice how the first type variable of `Named`{.haskell} is used
to refer to the key and map arguments.

You might be thinking that you understand how an argument of type
`Member keyName mapName`{.haskell} can be interpreted as above but
that you fail to see how this ensures the precondition. What prevents
the user from applying `lookup 2 (singleton 1 "a") Member`{.haskell}?
The answer is in the export list at the top. The data constructor
`Member`{.haskell} is not exported from the library prohibiting the
user from using it. They can only use `lookup`{.haskell} if they used
the function `member`{.haskell} before because only `member`{.haskell}
returns the `Member`{.haskell} value that they need but cannot
construct themselves. A value of type `Member keyName
mapName`{.haskell} can be interpreted as a proof that any key inside a
`Named keyName`{.haskell} wrapper is in any map inside a `Named
mapName`{.haskell} wrapper.

But there is another problem. What stops the user from writing
something like the following?

```haskell
let
  key1 :: Named keyName1 Integer
  key1 = 1
  key2 :: Named keyName2 Integer
  key2 = 2
  map :: Named mapName (Map Integer String)
  map = singleton 1 "a"
in
  case member key1 map of
    Nothing -> "" -- will not happen because key1 is in map
    Just membershipProof -> lookup key2 map membershipProof
```

Sure, we are using `membershipProof`{.haskell} of type `Member
keyName1 map`{.haskell} where the type `Member keyName2 map`{.haskell}
is expected. But `keyName1`{.haskell} and `keyName2`{.haskell} are
type variables. They can they can unify.

> Sorry, I did not manage to finish this paper yet.

# References
